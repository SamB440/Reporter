package net.islandearth.reporter.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.reporter.Reporter;
import net.islandearth.reporter.entry.ReportEntry;
import net.islandearth.reporter.translation.Translations;
import net.islandearth.reporter.ui.ReportMainInventory;
import net.islandearth.reporter.ui.ReportManageInventory;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@CommandAlias("report")
@Description("Report command")
public class Report extends BaseCommand {

	private final Reporter plugin;
	
	public Report(Reporter plugin) {
		this.plugin = plugin;
	}

	@Subcommand("reports|list")
	@CommandPermission("report.view.reports")
	public void onView(Player player) {
		new ReportManageInventory(plugin, player).open();
	}

	@Subcommand("reports|list")
	@CommandPermission("report.view.reports")
	public void onView(Player player, String offlinePlayer) {
		new ReportManageInventory(plugin, player, Bukkit.getOfflinePlayer(offlinePlayer)).open();
	}

	@Default
	@CommandCompletion("@players")
	@CommandPermission("report.report")
	public void onDefault(Player player, String offlinePlayer, String reason) {
		OfflinePlayer op = Bukkit.getOfflinePlayer(offlinePlayer);
		if (op.hasPlayedBefore() || op.isOnline()) {
			if (!op.getUniqueId().equals(player.getUniqueId())) {
				if (!player.hasPermission("report.bypasslimit")) {
					for (ReportEntry entry : Reporter.getAPI().getReportCache().getReports()) {
						if (entry.getReported().getUniqueId().equals(op.getUniqueId())
								&& entry.getReporter().getUniqueId().equals(player.getUniqueId())
								&& entry.getStatus() == ReportEntry.EntryStatus.OPEN) {
							Translations.ALREADY_REPORTED.send(player);
							return;
						}
					}
				}

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
				String time = LocalDateTime.now().format(formatter);
				Reporter.getAPI().getReportCache().getReports().add(new ReportEntry(player, op, reason, time, ReportEntry.EntryStatus.OPEN));
				Translations.REPORT_ENTRY.send(player, player.getName());
				Bukkit.getOnlinePlayers().forEach(oPlayer -> {
					if (oPlayer.hasPermission("report.view.new")) {
						Translations.NEW_REPORT.send(oPlayer, player.getName(), op.getName(), reason);
					}
				});
			} else Translations.CANNOT_REPORT_SELF.send(player);
		} else Translations.PLAYER_DOES_NOT_EXIST.send(player);
	}

	@Default
	@CommandPermission("report.report")
	public void execute(Player player) {
		new ReportMainInventory(plugin, player).open();
	}
}
