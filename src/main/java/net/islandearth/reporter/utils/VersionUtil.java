package net.islandearth.reporter.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.UUID;

public class VersionUtil {
	
	public static ItemStack getSkull(OfflinePlayer player) {
		return new ItemStackBuilder(XMaterial.PLAYER_HEAD.parseMaterial(true))
				.withName(player.getName())
				.withSkullOwner(player)
				.build();
	}
	
	public static ItemStack getSkull(String url)  {
        ItemStack head = getSkullItem();
        if (url.isEmpty()) return head;
        
    	SkullMeta headMeta = (SkullMeta) head.getItemMeta();
    	GameProfile profile = new GameProfile(UUID.randomUUID(), null);
    	byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
    	profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
    	Field profileField = null;
    	
    	try {
    		profileField = headMeta.getClass().getDeclaredField("profile");
    		profileField.setAccessible(true);
    		profileField.set(headMeta, profile);
    	} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
    		e1.printStackTrace();
    	}
         
    	head.setItemMeta(headMeta);
        
        return head;
	}
	
	public static ItemStack getSkullItem() {
		return new ItemStack(XMaterial.PLAYER_HEAD.parseMaterial(true));
	}
}
